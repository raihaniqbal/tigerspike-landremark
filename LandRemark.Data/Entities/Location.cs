﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LandRemark.Data.Infrastructure;

namespace LandRemark.Data.Entities
{
    public class Location : IEntity
    {
        public Location()
        {
            Remarks = new List<LocationRemark>();
        }

        [Key]
        public int ID { get; set; }

        public string Username { get; set; }

        public string Address { get; set; }

        public virtual ICollection<LocationRemark> Remarks { get; set; }

        public DateTime AddedOn { get; set; }
    }
}
