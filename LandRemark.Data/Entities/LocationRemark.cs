﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LandRemark.Data.Infrastructure;

namespace LandRemark.Data.Entities
{
    public class LocationRemark : IEntity
    {
        [Key]
        public int ID { get; set; }

        public string Remark { get; set; }

        public virtual Location Location { get; set; }
    }
}
