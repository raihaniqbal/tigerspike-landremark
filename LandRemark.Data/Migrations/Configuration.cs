namespace LandRemark.Data.Migrations
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LandRemark.Data.LandmarkRemarkContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(LandRemark.Data.LandmarkRemarkContext context)
        {
            Location location = new Location();
            location.AddedOn = DateTime.Now;
            location.Address = "34 Copeland St, Liverpool NSW 2170, Australia";
            location.Username = "tiger";

            LocationRemark remark = new LocationRemark();
            remark.Location = location;
            remark.Remark = "I love this place";

            context.LocationRemarkSet.AddOrUpdate(remark);

            base.Seed(context);
        }
    }
}
