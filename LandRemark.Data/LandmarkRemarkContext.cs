﻿using LandRemark.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandRemark.Data
{
    public class LandmarkRemarkContext : DbContext
    {
        public LandmarkRemarkContext() : base("name=DefaultConnection")
        { }

        public IDbSet<Location> LocationSet { get; set; }
        public IDbSet<LocationRemark> LocationRemarkSet { get; set; }
    }
}
