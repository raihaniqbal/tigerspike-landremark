﻿'use strict';
app.factory('noteService', ['$http', '$q', function ($http, $q) {

    function getNotesByLocation(location) {
        
        return DoGet('/api/Notes/get-by-location?location=' + location);
    }

    function getNotesByUsername(username) {
        
        return DoGet('/api/Notes/get-by-username?username=' + username);
    }

    function addNote(note) {
        return DoPost('/api/Notes/Add', note);
    }

    function DoGet(url)
    {
        var deferred = $q.defer();

        $http.get(url).then(function (response) {

            deferred.resolve(response.data);

        }, function (error) {

            deferred.reject(error);
        });

        return deferred.promise;
    }

    function DoPost(url, data, successCallback, errorCallback)
    {
        var deferred = $q.defer();

        $http.post(url, data).then(function (response) {

            deferred.resolve(response.data);

        }, function (error) {

            deferred.reject(error);
        });

        return deferred.promise;
    }

    return {
        getNotesByLocation: getNotesByLocation,
        getNotesByUsername: getNotesByUsername,
        addNote: addNote
    };

}]);