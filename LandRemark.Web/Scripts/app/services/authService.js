﻿'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/';

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });

    };

    var isAuthenticated = function () {
        return _authentication.isAuth;
    }

    var _login = function (loginData) {

        var data = "grant_type=password&userName=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).then(function (response) {

            localStorageService.set('authorizationData', { token: response.data.access_token, userName: response.data.userName, refreshToken: "", useRefreshTokens: false });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }, function (error) {
            _logOut();
            deferred.reject(error);
        });

        return deferred.promise;

    };

    var _loginExternal = function (externalData) {
        localStorageService.set('authorizationData', { token: externalData.access_token, userName: externalData.userName, refreshToken: "", useRefreshTokens: false });

        _authentication.isAuth = true;
        _authentication.userName = externalData.userName;
    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    };

    var getUserInfo = function () {
        return _authentication;
    }

    return {
        saveRegistration: _saveRegistration,
        login: _login,
        loginExternal: _loginExternal,
        logOut: _logOut,
        fillAuthData: _fillAuthData,
        isAuthenticated: isAuthenticated,
        getUserInfo: getUserInfo
    };

}]);