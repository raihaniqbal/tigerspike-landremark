﻿'use strict';
app.controller('registerCtrl', registerCtrl);

registerCtrl.$inject = ['$scope', 'authService', '$rootScope', '$location'];

function registerCtrl($scope, authService, $rootScope, $location) {
    $scope.register = register;
    $scope.registerData = {};

    function register() {
        authService.saveRegistration($scope.registerData).then(function (response) {
            $location.path('/login');
        }, function (error) {
            
        });
    }
}