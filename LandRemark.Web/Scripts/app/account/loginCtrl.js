﻿'use strict';
app.controller('loginCtrl', loginCtrl);

loginCtrl.$inject = ['$scope', 'authService', '$rootScope', '$location'];

function loginCtrl($scope, authService, $rootScope, $location) {
    $scope.submitLogin = login;
    $scope.loginData = {};

    function login() {
        authService.login($scope.loginData).then(function (response) {
            redirectUser();
        }, function (error) {
            alert('Invalid username or password');
        });
    }

    $scope.authExternalProvider = function (provider) {
        var redirectUri = location.protocol + '//' + location.host + '/Scripts/app/account/externalAuthSuccess.html';

        var externalProviderUrl = "/api/Account/ExternalLogin?provider=" + provider
                                                                    + "&response_type=token&client_id=LandmarkRemark"
                                                                    + "&redirect_uri=" + redirectUri;
        window.$windowScope = $scope;

        $scope.showOAuthWindow(externalProviderUrl, "Authenticate Account");
    };

    $scope.showOAuthWindow = function (externalProviderUrl, title) {
        var w = 600, h = 600;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;

        var newWindow = window.open(externalProviderUrl, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    $scope.authCompletedCB = function (fragment) {
        authService.loginExternal(fragment);
        redirectUser();
    };

    function redirectUser()
    {
        if (authService.isAuthenticated()) {

            $scope.$apply(function () {
                $scope.userData.loadUserInfo();

                if ($rootScope.previousState)
                    $location.path($rootScope.previousState);
                else
                    $location.path('/');
            });
        }
        else {
            alert('Login failed. Try again.');
        }
    }
}