﻿'use strict';

app.controller('indexCtrl', indexCtrl);

indexCtrl.$inject = ['$scope', 'noteService', 'authService'];

function indexCtrl($scope, noteService, authService, NgMap) {

    $scope.mapLocation = 'current-location';
    $scope.curretLocationNotes = [];
    $scope.noteData = {};

    $scope.$on('mapInitialized', function (event, map) {
        geocodeLatLng(map)
    });

    $scope.$watch('noteData', function (noteData) {
        if (noteData.Address != "")
        {
            noteService.getNotesByLocation(noteData.Address).then(function (notes) {
                $scope.curretLocationNotes = notes;
            });
        }
    }, true);

    $scope.submitRemark = function()
    {
        $scope.noteData.Username = authService.getUserInfo().userName;
        noteService.addNote($scope.noteData).then(function (response) {
            $scope.curretLocationNotes.push(response);
            userNoteForm.reset();
        });
    }

    function geocodeLatLng(map) {
        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;
        geocoder.geocode({ 'location': map.center }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    map.setZoom(14);
                    var marker = new google.maps.Marker({
                        position: map.center,
                        map: map
                    });
                    $scope.noteData.Address = results[0].formatted_address;
                    $scope.$digest();
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
}