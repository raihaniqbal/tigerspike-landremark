﻿(function () {
	'use strict';

	angular.module('common.core', ['ngRoute', 'ngCookies', 'angularValidator', 'LocalStorageModule']);

})();