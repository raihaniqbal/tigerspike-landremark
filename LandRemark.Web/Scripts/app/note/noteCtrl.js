﻿'use strict';

app.controller('noteCtrl', noteCtrl);

noteCtrl.$inject = ['$scope', 'noteService'];

function noteCtrl($scope, noteService) {
    $scope.notes = [];

    noteService.getNotesByUsername($scope.userData.username).then(function (notes) {
        $scope.notes = notes;
    });
}