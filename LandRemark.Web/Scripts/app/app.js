﻿'use strict';

var app = angular.module('landmarkRemark', ['common.core', 'common.ui'])
    .config(config)
    .run(run);

isAuthenticated.$inject = ['authService', '$rootScope', '$location'];

function isAuthenticated(authService, $rootScope, $location) {
    if (!authService.isAuthenticated()) {
        $rootScope.previousState = $location.path();
        $location.path('/login');
    }
}

config.$inject = ['$routeProvider'];
function config($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "Scripts/app/home/index.html",
            controller: "indexCtrl"
        })
        .when("/login", {
            templateUrl: "Scripts/app/account/login.html",
            controller: "loginCtrl"
        })
        .when("/register", {
            templateUrl: "Scripts/app/account/register.html",
            controller: "registerCtrl"
        })
        .when("/notes/my", {
            templateUrl: "Scripts/app/note/notes.html",
            controller: "noteCtrl",
            resolve: { isAuthenticated: isAuthenticated }
        })
        .otherwise({ redirectTo: "/" });
}

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

run.$inject = ['authService'];
function run(authService) {
    // handle page refreshes
    authService.fillAuthData();
}


