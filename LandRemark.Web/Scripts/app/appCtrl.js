﻿app.controller('appCtrl', appCtrl);

appCtrl.$inject = ['$scope', '$location', 'authService', '$rootScope'];
function appCtrl($scope, $location, authService, $rootScope) {

    $scope.userData = {};

    $scope.userData.loadUserInfo = loadUserInfo;
    $scope.logout = logout;


    function loadUserInfo() {
        $scope.userData.isUserLoggedIn = authService.isAuthenticated();

        if ($scope.userData.isUserLoggedIn) {
            $scope.userData.username = authService.getUserInfo().userName;
        }
    }

    function logout() {
        authService.logOut();
        $location.path('#/');
        $scope.userData.loadUserInfo();
    }

    $scope.userData.loadUserInfo();
}