﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace LandRemark.Web.Models
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class AuthRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(RegisterViewModel model)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = model.Email
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            return result;
        }

        public async Task<IdentityResult> RegisterExternalUser(IdentityUser user)
        {
            var result = await _userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddExternalLogin(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        public async Task<IdentityUser> FindUser(LoginViewModel model)
        {
            IdentityUser user = await _userManager.FindAsync(model.Email, model.Password);
            
            return user;
        }

        public async Task<IdentityUser> FindByEmail(string email)
        {
            IdentityUser user = await _userManager.FindByEmailAsync(email);
            return user;
        }

        public async Task<IdentityUser> FindAsync(UserLoginInfo loginInfo)
        {
            IdentityUser user = await _userManager.FindAsync(loginInfo);

            return user;
        }
        
        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}