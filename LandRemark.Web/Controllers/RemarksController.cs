﻿using LandRemark.Data;
using LandRemark.Data.Entities;
using LandRemark.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LandRemark.Web.Controllers
{
    [RoutePrefix("api/Notes")]
    public class RemarksController : ApiController
    {
        public LandmarkRemarkContext _context = null;

        public RemarksController()
        {
            _context = new LandmarkRemarkContext();
        }

        [AllowAnonymous]
        [Route("get-by-location")]
        public IHttpActionResult getRemarksByLocation(string location)
        {
            var remarks = _context.LocationRemarkSet
                .Where(l => l.Location.Address.Contains(location))
                .Select(l => new LocationViewModel
                {
                    AddedOn = l.Location.AddedOn,
                    Address = l.Location.Address,
                    Remarks = l.Remark,
                    Username = l.Location.Username
                });

            return Ok(remarks);
        }

        [Authorize]
        [Route("get-by-username")]
        public IHttpActionResult getRemarksByUsername(string username)
        {
            var remarks = _context.LocationRemarkSet
                .Where(l => l.Location.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                .Select(l => new LocationViewModel
                {
                    AddedOn = l.Location.AddedOn,
                    Address = l.Location.Address,
                    Remarks = l.Remark,
                    Username = l.Location.Username
                });

            return Ok(remarks);
        }

        [HttpPost]
        [Route("Add")]
        public IHttpActionResult addNote(LocationViewModel model)
        {
            Location location = _context.LocationSet.Where(l => l.Address.Equals(model.Address, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

            if (location == null)
            {
                location = new Location
                {
                    AddedOn = DateTime.Now,
                    Address = model.Address,
                    Username = model.Username
                };
                _context.LocationSet.Add(location);
            }

            location.Remarks.Add(new LocationRemark { Remark = model.Remarks });
            _context.SaveChanges();

            return Ok(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}