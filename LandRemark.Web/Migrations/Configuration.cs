namespace LandRemark.Web.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LandRemark.Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(LandRemark.Web.Models.ApplicationDbContext context)
        {
            var manager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new ApplicationDbContext()));

            var user = new IdentityUser()
            {
                UserName = "tiger",
                Email = "tiger@spike.com",
                EmailConfirmed = true
            };

            manager.Create(user, "Sc00tTiger");
        }
    }
}
