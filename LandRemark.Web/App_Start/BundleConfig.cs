﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace LandRemark.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
                 "~/Scripts/jquery-3.1.1.js",
                 "~/Scripts/bootstrap.js",
                 "~/Scripts/angular.js",
                 "~/Scripts/angular-route.js",
                 "~/Scripts/angular-cookies.js",
                 "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                 "~/Scripts/ng-map.min.js",
                 "~/Scripts/angular-local-storage.js",
                 "~/Scripts/angular-validator.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/spa").Include(
                "~/Scripts/app/modules/common.core.js",
                "~/Scripts/app/modules/common.ui.js",
                "~/Scripts/app/app.js",
                "~/Scripts/app/services/authInterceptorService.js",
                "~/Scripts/app/services/authService.js",
                "~/Scripts/app/services/noteService.js",
                "~/Scripts/app/layout/topBar.directive.js",
                "~/Scripts/app/layout/sideBar.directive.js",
                "~/Scripts/app/account/loginCtrl.js",
                "~/Scripts/app/account/registerCtrl.js",
                "~/Scripts/app/note/noteCtrl.js",
                "~/Scripts/app/appCtrl.js",
                "~/Scripts/app/home/indexCtrl.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/bootstrap.css",
                 "~/Content/bootstrap-theme.css",
                 "~/Content/bootstrap-social.css",
                 "~/Content/site.css"));
        }
    }
}
