﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using LandRemark.Web.Providers;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Facebook;

[assembly: OwinStartup(typeof(LandRemark.Web.Startup))]

namespace LandRemark.Web
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthServerOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new SimpleAuthorizationServerProvider()
            };

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            //app.UseOAuthBearerTokens(OAuthServerOptions); // this encapsulates the above two lines

            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);

            //Configure Google External Login
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "915269487110-t1vvpagl7c7l8ar11lrgm9h1v2fr1o2f.apps.googleusercontent.com",
                ClientSecret = "Ewh_uGEtFO77YY6Ls3pn0-hI",
                Provider = new GoogleAuthProvider()
            });

            //Configure Facebook External Login
            var facebookAuthOptions = new FacebookAuthenticationOptions()
            {
                AppId = "1574953795862490",
                AppSecret = "a167817d25ae2c76fdf0ab6955fa389e",
                Provider = new FacebookAuthProvider(),
                UserInformationEndpoint = "https://graph.facebook.com/v2.9/me?fields=id,name,email,first_name,last_name"
            };
            facebookAuthOptions.Scope.Add("email");
            app.UseFacebookAuthentication(facebookAuthOptions);
        }
    }
}