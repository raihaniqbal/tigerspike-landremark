### What is this repository for? ###

“Landmark Remark” - allows users to save location based notes on a map. These notes can be displayed on the map where they were saved and
viewed by the user that created the note as well as other users of the application.

### How do I get set up? ###

* Checkout the code
* Build Solution
* Make sure **LandRemark.Web** project is set to default
* Run the application


### Who do I talk to? ###

Send all queries to me@raihaniqbal.net